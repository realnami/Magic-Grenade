all:
	@vim -c "VimwikiAll2HTML" -c "qall" src/index.wiki 

graph:
	@dot -T svg src/經歷.dot > public/經歷.html

.PHONY: test-server
test-server:
	python3 -m http.server --bind 127.0.0.1 --directory public 8080
